//
//  WeatherListerUITests.swift
//  WeatherListerUITests
//
//  Created by Fergal on 29/08/2019.
//  Copyright © 2019 Mohan Digital. All rights reserved.
//

import XCTest

class WeatherListerUITests: XCTestCase {

    var xcApp: XCUIApplication?
    let kExpectedAccessibilityIdentifierForFirstCell:String = "STF Vandrarhem Stigbergsliden"   // TODO make more bullet-proof
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        self.xcApp = XCUIApplication();
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        self.xcApp!.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        if let cells = self.xcApp?.cells{
            XCTAssert(cells.count > 0)
            let cell = cells.element(matching: XCUIElement.ElementType.cell, identifier: kExpectedAccessibilityIdentifierForFirstCell)
            if cell.isHittable{
                // double check the cell
                XCTAssert(cell.identifier.contains(kExpectedAccessibilityIdentifierForFirstCell) )
                // Sanity check that it's the first Cell which is named based on the Server Response matches the expected accessibiltyLabel
                XCTAssert(cells.firstMatch.identifier.contains(kExpectedAccessibilityIdentifierForFirstCell) )      // make sure we found a match on the first one
                cell.doubleTap()    // seems to need one tap to focus and one the select
//                XCUIApplication().scrollViews.containing(.staticText, identifier:kExpectedAccessibilityIdentifierForFirstCell).element.tap()
                sleep(2)
                if let detailsScrollView = self.xcApp?.scrollViews{
                    XCTAssert(detailsScrollView.count > 0)
                    let query = detailsScrollView.children(matching: .staticText)
                    let nameLabel = query.element.firstMatch.label
                    //kExpectedAccessibilityIdentifierForFirstCell
                    XCTAssert(nameLabel.contains(kExpectedAccessibilityIdentifierForFirstCell) )
                }
            }
        }
    }
}
