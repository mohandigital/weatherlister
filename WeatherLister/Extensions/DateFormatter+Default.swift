//
//  DateFormatter+default.swift
//  ConnfaCore
//
//  Created by Marian Fedyk on 8/28/17.
//

import Foundation

let defaultFormatter = DateFormatter.default

extension DateFormatter {
  static var `default`: DateFormatter {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssxxxxx"
    formatter.locale = Locale(identifier:TimeZone.current.identifier)
    formatter.timeZone = TimeZone.current   // use current if not overridden
    return formatter
  }
}
