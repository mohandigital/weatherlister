//
//  DetailViewController.swift
//  WeatherLister
//
//  Created by Fergal on 26/03/2020.
//  Copyright © 2019 Mohan Digital. All rights reserved.
//

import UIKit
import MapKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!
    @IBOutlet var weatherDescription: UILabel!
    @IBOutlet var weatherImageView: UIImageView!
    @IBOutlet var weatherAddress: UILabel!
    @IBOutlet var weatherMapView: MKMapView!
    
    var ownerDetailItem: WeatherDetails?
    var selectedIndex:Int?
    var cachedImage: UIImage?

    var dataManager : ListDataManager!      // supplied by parent (Master) which is destined to stick around either on VC Stack or in Splitter


    func configureView() {
        // Update the user interface for the detail item.
        if let detail = ownerDetailItem {
            if let label = detailDescriptionLabel {
                label.text = detail.city.name
            }
            if let metaDetails = detailItem{
                if let label = weatherDescription {
                    label.text = metaDetails.dt_txt
                }
                if let label = weatherAddress {
                    label.text = String(metaDetails.dt)
                }
            }
            if let weatherImageView = weatherImageView{
                // just show thw one we've cached as a placeholder first
                weatherImageView.image = self.cachedImage
            }
            // TODO set Mao to center on detail.latitude detail.longitude
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(dataManager != nil){
            if let detail = detailItem {
                if(self.dataManager.cachedWeatherDetailsDictionary == nil){
                    self.dataManager.cachedWeatherDetailsDictionary = detail
                }
                if(detail.dt != self.dataManager.cachedWeatherDetailsDictionary.dt){
                    self.dataManager.cachedWeatherDetailsDictionary = detail
                }
                self.configureView()
            }
        }
    }
    var detailItem: WeatherMetaDetails? {
        didSet {
            // Update the view.
            configureView()
        }
    }


}

