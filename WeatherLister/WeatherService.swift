//
//  WeatherService.swift
//  WeatherLister
//
//  Created by Fergal on 26/03/2020.
//  Copyright © 2019 Mohan Digital. All rights reserved.
//

import Foundation

class WeatherService{
    static func getWeatherInfoDataForMyCity(completion: @escaping (_ weather: Data?, _ error: Error?) -> Void) {
        // City is hard wired for test so use Prefab URL for city 
        let mockDataURL = "http://api.openweathermap.org/data/2.5/forecast?q=Drogheda&appid=544cc5775fb30918dd9276e161a8dbff"
        getJSONFromURL(urlString: mockDataURL) { (data, error) in
            guard let data = data, error == nil else {
                return completion(nil, error)
            }
            return completion(data, error)
        }
    }
    
    static func getDetailsDataForWeatherID(weatherID:String, completion: @escaping (_ weather: Data?, _ error: Error?) -> Void) {
        // Append the WeatherID of interest to the Prefab URL for city
        let mockDetailsDataURL = "http://api.openweathermap.org/data/2.5/forecast?q=\(weatherID)&appid=544cc5775fb30918dd9276e161a8dbff"
        getJSONFromURL(urlString: mockDetailsDataURL) { (data, error) in
            guard let data = data, error == nil else {
                return completion(nil, error)
            }
            return completion(data, error)
        }
    }

    static func getJSONFromURL(urlString: String, completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        guard let url = URL(string: urlString) else {
            print("Error: Cannot create URL from mockDataURL string")
            return
        }
        let urlRequest = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, _, error) in
            guard error == nil else {
                print("Error calling Service")
                return completion(nil, error)
            }
            guard let responseData = data else {
                print("Data is nil")
                return completion(nil, error)
            }
            completion(responseData, nil)
        }
        task.resume()
    }
}


