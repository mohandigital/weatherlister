//
//  ListDataManager.swift
//  ListMania
//
//  Created by Fergal on 26/03/2020.
//  Copyright © 2019 Mohan Digital. All rights reserved.
//

import Foundation

class ListDataManager
{
    var WeatherDetailsList : WeatherDetails!
    var cachedWeatherDetailsDictionary : WeatherMetaDetails!
    var backingStoreFileName : String
    

    init(filenameForBackingStore:String) {
        backingStoreFileName = filenameForBackingStore      //
    }
    
    func readData(completion: @escaping (_ weather: Data?, _ error: Error?) -> Void)  {
        WeatherService.getWeatherInfoDataForMyCity() { (remoteJSONData, error) in
            if let error = error {
                print("Get WeatherInfo reports error: \(error.localizedDescription)")
                return completion(remoteJSONData, error)
            }
            if let remoteJSONData = remoteJSONData {
                do {
                    let decoder = JSONDecoder()
                    
                    self.WeatherDetailsList = try decoder.decode(WeatherDetails.self, from:remoteJSONData)
                    print("Found \(self.WeatherDetailsList.city) Weather for My City")
                    return completion(remoteJSONData, error)
                } catch let parseError {
                    print(parseError)
                }   // END DO
            }
            else{
                print("JSON Data Retrieval Failed to materialize a payload but no error was reported")
                return completion(remoteJSONData, error)       // empty data
            }
        }
    }
    func readDetailsFor(weatherID:String, completion: @escaping (_ weather: Data?, _ error: Error?) -> Void)  {
        WeatherService.getDetailsDataForWeatherID(weatherID:weatherID) { (remoteJSONData, error) in
            if let error = error {
                print("Get WeatherInfo Details reports error: \(error.localizedDescription)")
                return completion(remoteJSONData, error)
            }
            if let remoteJSONData = remoteJSONData {
                do {
                    let decoder = JSONDecoder()
                    
                    let weatherDetailsDictionary = try decoder.decode(WeatherMetaDetails.self, from:remoteJSONData)
                    self.cachedWeatherDetailsDictionary = weatherDetailsDictionary;
                    print("Found Detailed Weather for ID /(weatherID)")
                    return completion(remoteJSONData, error)
                } catch let parseError {
                    print(parseError)
                }   // END DO
            }
            else{
                print("JSON Data Retrieval Failed to materialize a payload but no error was reported")
                return completion(remoteJSONData, error)       // empty data
            }
        }
    }

}
