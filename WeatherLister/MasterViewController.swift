//
//  MasterViewController.swift
//  WeatherLister
//
//  Created by Fergal on 26/03/2020.
//  Copyright © 2019 Mohan Digital. All rights reserved.
//

import UIKit

let kKelvinOffset:Float = 273.15


class MasterViewController: UITableViewController {

    
    var detailViewController: DetailViewController? = nil
    //var objects = [Any]()
    var dataManager : ListDataManager!
    var lastSelectedImage:UIImage? = nil
    
    // for cell tags
    enum weatherCellTags:Int {
        case weatherNameTag = 1, weatherTypeTag, weatherTemperatureTag, weatherHeatTag, weatherThumbnailTag
    }
    func colorForHeatLevel(heatValue:Int) -> UIColor {
        var weatherHeatTextColor: UIColor
        if(heatValue <= 0){
            weatherHeatTextColor = UIColor.gray
        }
        else if(heatValue >= 15){
            weatherHeatTextColor = UIColor.red
        }
        else if(heatValue >= 10){
            weatherHeatTextColor = UIColor.orange
        }
        else if(heatValue >= 5){
            weatherHeatTextColor = UIColor.yellow
        }
        else if(heatValue > 0){
            weatherHeatTextColor = UIColor.blue
        }
        else{
            weatherHeatTextColor = UIColor.black
        }
        return weatherHeatTextColor;
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationItem.leftBarButtonItem = editButtonItem

        let addButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refreshWeatherInfoForCity(_:)))
        navigationItem.rightBarButtonItem = addButton
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        // Do any additional setup after loading the view. TODO remove Backing Store File used for Testing
        dataManager = ListDataManager(filenameForBackingStore:"/Users/fergal/fadetest.txt")
    }

    override func viewWillAppear(_ animated: Bool) {
        //clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
        if(dataManager != nil){
            dataManager.readData(){ (remoteJSONData, error) in
                if (error == nil){
                    print("Completed Retrieval of Data")
                    DispatchQueue.main.async{
                        self.tableView.reloadData()
                    }
                }
                else{
                    print("Give user the feedback of why ReadData Failed reports error: \(error!.localizedDescription)")
                }
            }
        }
    }

    @objc
    func refreshWeatherInfoForCity(_ sender: Any) {
        tableView.reloadData()
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                if(indexPath.row < dataManager.WeatherDetailsList.cnt)
                {
                    let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                    // TODO allow a list of cities to be mananged (weather near me, + my Favourites)
                    if let weatherMetaObject = dataManager.WeatherDetailsList.list[indexPath.row] as WeatherMetaDetails?{
                        controller.ownerDetailItem = dataManager.WeatherDetailsList
                        controller.detailItem = weatherMetaObject                        
                        controller.cachedImage = self.imageFromCellIndexPath(indexPath: indexPath)
                    }
                    controller.dataManager = self.dataManager       // pas in our dataManager instnace
                    if let selectedImage = self.lastSelectedImage{
                        controller.cachedImage = selectedImage
                    }
                    controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                    controller.navigationItem.leftItemsSupplementBackButton = true
                }
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount:Int = 0
        if let weatherInfoArrayForSelectedCity = dataManager.WeatherDetailsList{
            rowCount = weatherInfoArrayForSelectedCity.cnt
        }
        return rowCount
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        if(indexPath.row < dataManager.WeatherDetailsList.cnt)
        {
            // TODO allow a list of cities to be mananged (weather near me, + my Favourites)
            // using [indexPath.row]
            let weatherMetaObject = dataManager.WeatherDetailsList.list[indexPath.row] as WeatherMetaDetails
            //wire the IBOutlets
            if let weatherName = cell.viewWithTag(weatherCellTags.weatherNameTag.rawValue) as? UILabel{
                weatherName.text = weatherMetaObject.dt_txt
                // set the accessibilityIdentifier so that it is more useful to App Users and can be sanity checked by UI Tests
                cell.accessibilityIdentifier = weatherMetaObject.dt_txt
            }
            if let weatherType = cell.viewWithTag(weatherCellTags.weatherTypeTag.rawValue) as? UILabel{
                weatherType.text = weatherMetaObject.weather[0].main
            }
            if let weatherDetails = cell.viewWithTag(weatherCellTags.weatherTemperatureTag.rawValue) as? UILabel{
                weatherDetails.text = weatherMetaObject.weather[0].description
            }
            if let weatherTemperature = cell.viewWithTag(weatherCellTags.weatherHeatTag.rawValue) as? UILabel{
                let tempInCelcius = weatherMetaObject.main.temp - kKelvinOffset
                weatherTemperature.text = String(format:"%.1f", tempInCelcius)
                let temperatureHeatValue = Int(tempInCelcius)
                let weatherHeatHexTextColor = self.colorForHeatLevel(heatValue: temperatureHeatValue)
                    weatherTemperature.textColor = weatherHeatHexTextColor;
            }
            // and finally Lazily Load the images
            if let imageURLString = dataManager.WeatherDetailsList.imageURLStringForWeatherAtIndex(index:indexPath.row){
                cell.imageView?.downloadImageFrom(link: imageURLString, contentMode: UIView.ContentMode.scaleAspectFit)
            }
            cell.imageView?.image = UIImage(named:"calendarDate.png")
            cell.accessoryType = .detailDisclosureButton
//            let checkImage:UIImage = UIImage(named: "calendarDate")!
//            if let dateCheckImage:UIImage = self.composite(calendarDate:dateForThumbnail, onImage:checkImage){
//                cell.imageView?.image = dateCheckImage
//            }
//            else{
//                cell.imageView?.image = checkImage
//            }

        }
        return cell
    }
    
    func imageFromCellIndexPath(indexPath:IndexPath) -> UIImage? {
        var weatherImage:UIImage? = nil
        let cell = self.tableView(self.tableView, cellForRowAt: indexPath)
        if let weatherThumbnailImageView = cell.viewWithTag(weatherCellTags.weatherThumbnailTag.rawValue) as? UIImageView{
            weatherImage = weatherThumbnailImageView.image
        }
        return weatherImage
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // lazy re-download the full image so we can lazily update
        self.lastSelectedImage = self.imageFromCellIndexPath(indexPath: indexPath)
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }

//    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//            objects.remove(at: indexPath.row)
//            tableView.deleteRows(at: [indexPath], with: .fade)
//        } else if editingStyle == .insert {
//            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
//        }
//    }

    // MARK: - utility Image Helper functions
    func textToImage(drawText text: String, textSize:Float, textColor:UIColor, onImage image: UIImage, atPoint point: CGPoint) -> UIImage? {
        let textFont = UIFont.systemFont(ofSize: CGFloat(textSize))
        
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        
        let textFontAttributes = [
            NSAttributedString.Key.font: textFont,
            NSAttributedString.Key.foregroundColor: textColor,
            ] as [NSAttributedString.Key : Any]
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        
        let rect = CGRect(origin: point, size: image.size)
        text.draw(in: rect, withAttributes: textFontAttributes)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    func composite(calendarDate:Date?, onImage image: UIImage) -> UIImage? {
        var newImage:UIImage?
        if let date = calendarDate {
            let formatter = DateFormatter.default
            formatter.dateFormat = "MMM"
            let month = formatter.string(from: date)
            if let updatedImage = self.textToImage(drawText:month, textSize:13, textColor:UIColor.white, onImage:image, atPoint: CGPoint(x: 20, y: 7)){
                formatter.dateFormat = "dd"
                let day = formatter.string(from: date)
                newImage = self.textToImage(drawText:day, textSize:19, textColor:UIColor.red, onImage:updatedImage, atPoint: CGPoint(x: 20, y: 20))
            }
        }
        return newImage
    }

}   // END class

// Extension to allow the images to be loaded lazily
extension UIImageView {
    func downloadImageFrom(link:String, contentMode: UIView.ContentMode) {
        URLSession.shared.dataTask( with: NSURL(string:link)! as URL, completionHandler: {
            (data, response, error) -> Void in
            DispatchQueue.main.async {
                self.contentMode =  contentMode
                if let data = data { self.image = UIImage(data: data) }
            }
        }).resume()
    }
}

