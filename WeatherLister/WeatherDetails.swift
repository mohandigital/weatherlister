//
//  WeatherDetails.swift
//  WeatherLister
//
//  Created by Fergal on 26/03/2020.
//  Copyright © 2019 Mohan Digital. All rights reserved.
//

import Foundation

class Coord:Codable{
    let lat:Float
    let lon:Float
}

class CityDetails:Codable{
        let id:Int
        let name:String
        let coord:Coord
    let country:String
}

class WeatherDetails: Codable {
    let cod:String
    let message:Float
    let cnt:Int
    let list:Array<WeatherMetaDetails>
    let city:CityDetails
//    let images:Array<Dictionary<String,String>>
    // optional weatherInfo 
    
    
    func imageURLStringForWeatherAtIndex(index:Int) -> String? {
        var imageURLString :String?
        // combine the prefix and suffix to create the URL from the Images Array
        if(index < self.cnt){
            let weatherIcon = self.list[index].weather[0].icon
            if weatherIcon.count > 0{
                imageURLString = "http://openweathermap.org/img/wn/\(weatherIcon)@2x.png"
            }
        }
        return imageURLString;
    }
}

class MainDetails: Codable{
    let temp:Float
    let temp_min:Float
    let temp_max:Float
    let pressure:Float
    let sea_level:Float
    let grnd_level:Float
    let humidity:Int
    let temp_kf:Float
}

class WeatherAbstract: Codable{
    let id: Int
    let main:String
    let description:String
    let icon:String
}

class WeatherMetaDetails: Codable {
    let dt:Int64
    let main:MainDetails
    let weather:Array<WeatherAbstract>
    let clouds:Dictionary<String,Int>
    let wind:Dictionary<String,Float>
    let snow:Dictionary<String,Float>?
    let sys:Dictionary<String,String>
    let dt_txt:String
    /*      {
            "dt": 1486220400,
            "main": {
                "temp": 260.26,
                "temp_min": 260.26,
                "temp_max": 260.26,
                "pressure": 1021,
                "sea_level": 1042.96,
                "grnd_level": 1021,
                "humidity": 86,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 56
            },
            "wind": {
                "speed": 2.47,
                "deg": 180.501
            },
            "snow": {},
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2017-02-04 15:00:00"
        }

     */
    
}
