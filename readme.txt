
Build:
Open WeatherLister.xcodeproj in Xcode 11.x
Build and Run the App on a Simulator
N.B. The AppID 544cc5775fb30918dd9276e161a8dbff
was setup for api.openweathermap.org

ToDo:

Add a Search Text Box to allow a User to input a Location for Weather
Add Location Manager to allow a User’s current Location to be determined
Reformat the Time/Date on the Master Screen to show in Calendar & Clock style images
Flesh out the Details Screen to show more details in a nicer format
Write some Unit Tests
Allow a list of cities to be remembered & managed (weather near me, + my Favourites)
Add some CocoaPods to incorporate Animations / ToolTips
